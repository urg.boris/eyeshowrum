﻿using UnityEngine;
using UnityEngine.PlayerLoop;

namespace EyeShowRum.Scripts
{
    public abstract  class ACameraState
    {
        protected CameraController cameraController;

        public ACameraState(CameraController cameraController)
        {
            this.cameraController = cameraController;
        }
        
        public virtual void Start()
        {
            
        }

        public virtual void Update()
        {
            
        }
        
        
        
        protected float ClampEulerAngle(float angle, float min, float max)
        {
            if (angle < -360)
                angle += 360;
            if (angle > 360)
                angle -= 360;
            return Mathf.Clamp(angle, min, max);
        }
    }
}