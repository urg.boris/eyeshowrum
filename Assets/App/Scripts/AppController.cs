﻿using System;
using System.Numerics;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Quaternion = UnityEngine.Quaternion;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

namespace EyeShowRum.Scripts
{
    public class AppController : MonoBehaviour
    {
        [SerializeField] private Camera mainCamera;
        [SerializeField] private MenuController menuController;
        [SerializeField] private MeshRenderer carMeshRenderer;
        [SerializeField] private CameraController cameraController;
        [SerializeField] private CarOptions carOptions;
        [SerializeField] private CarConfiguration carConfiguration;
        [SerializeField] private PartSwitcher wheelsSwitcher;
        [SerializeField] private PartSwitcher tunningPart1Switcher;
        
        private static readonly int Metallic = Shader.PropertyToID("_Metallic");

        void UpdatePrice()
        {
            float priceSum = carConfiguration.CountPrice();
            menuController.UpdatePrice(priceSum);
        }

        void SetInitState()
        {
            cameraController.SetInitState();
            menuController.SetOptions(carOptions);
            carConfiguration.SetInit();
            wheelsSwitcher.IsValid(carOptions.wheels.Length);
            tunningPart1Switcher.IsValid(carOptions.tuningPart1Options.Length);
        }

        private void Start()
        {
            SetInitState();
        }


        public void ChangeCarMaterialColor(int colorIndex)
        {
            carConfiguration.SetColorIndex(colorIndex);
            Color color = carOptions.colors[colorIndex].color;
            carMeshRenderer.sharedMaterial.color = color;
            UpdateGlossiness();
            UpdatePrice();
        }

        public void GlossinessValueChanged(Dropdown glossinesDropDown)
        {
            carConfiguration.SetGlossinessIndex(glossinesDropDown.value);
            UpdateGlossiness();
            UpdatePrice();
        }

        public void UpdateGlossiness()
        {
            var metalCoef = carOptions.glosinesses[carConfiguration.GlossinessIndex].metallicCoef;
            carMeshRenderer.sharedMaterial.SetFloat(Metallic, metalCoef);
        }

        public void WheelValueChanged(Dropdown dropdown)
        {
            carConfiguration.SetWheelIndex(dropdown.value);
            wheelsSwitcher.ChangePart(dropdown.value);
            UpdatePrice();
        }

        public void TunningPart1ValueChanged(Dropdown dropdown)
        {
            carConfiguration.SetTuningPart1Index(dropdown.value);
            tunningPart1Switcher.ChangePart(dropdown.value);
            UpdatePrice();
        }
    }
}