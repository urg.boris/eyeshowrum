﻿using UnityEngine;
using UnityEngine.Serialization;

namespace EyeShowRum.Scripts
{
    public class CameraController : MonoBehaviour
    {
        public Camera mainCamera;
        [SerializeField] private MenuController menuController;
        public Transform orbitingCenter;
        public float radius;
        public float angleSpeed;
        public float scrollSpeed;
        public Vector2 xAngleLimits;
        public Vector2 radiusLimits;
        public float smoothTime = 0.03F;
        
        private float yAngle;
        private float xAngle;

        public  Transform cameraTransform;
        private Vector3 velocity = Vector3.zero;

        private Vector3 lastMousePosition;
        private ACameraState currentCameraState;
        
        public void SetInitState()
        {
            SetState(new ZoomingCameraState(this));
        }

        private void Start()
        {
            cameraTransform = mainCamera.transform;
        }

        public void SetState(ACameraState cameraState)
        {
            if (currentCameraState==null)
            {
                // cameraState.Start();
                currentCameraState = cameraState;
                return;
            }
            Debug.Log(currentCameraState.GetType()+" != "+cameraState.GetType());
            if (currentCameraState.GetType() != cameraState.GetType())
            {
                // cameraState.Start();
                currentCameraState = cameraState;
            }
        }

        public void GetStartingOrbitAngles()
        {
            var angles = cameraTransform.eulerAngles;
            yAngle = angles.y;
            xAngle = angles.x;
        }
        
        public Vector2 GetStartOrbitAngles()
        {
            var angles = cameraTransform.eulerAngles;
            return new Vector2(angles.x, angles.y);
        }

        private void LateUpdate()
        {
            currentCameraState?.Update();
        }

        public bool IsMouseOverUI()
        {
            return menuController.IsMouseOver();
        }

        public void OrbitingUpdate()
        {
            radius -= Input.mouseScrollDelta.y * scrollSpeed;
            radius = Mathf.Clamp(radius, radiusLimits[0], radiusLimits[1]);
            if (Input.GetMouseButtonDown(0))
            {
                lastMousePosition = Input.mousePosition;
            }
            if (Input.GetMouseButton(0))
            {

                if (!menuController.IsMouseOver())
                {
                    var mouseDeltaPosition = (lastMousePosition - Input.mousePosition);
                    yAngle -= mouseDeltaPosition.x * angleSpeed * Time.deltaTime;
                    xAngle += mouseDeltaPosition.y * angleSpeed * Time.deltaTime;
                }
            }
            xAngle = ClampEulerAngle(xAngle, xAngleLimits[0], xAngleLimits[1]);
            var rotation = Quaternion.Euler(xAngle, yAngle, 0);
            var position = orbitingCenter.position + rotation * new Vector3(0.0f, 0.0f, -radius);
            cameraTransform.rotation = rotation;
            cameraTransform.position = position;
        }

        public void ZoomingUpdate()
        {
            var position = orbitingCenter.position;
            var targetPosition = position + Quaternion.Euler(xAngleLimits[0], 180, 0) * new Vector3(0.0f, 0.0f, -radius);
            
            cameraTransform.position = Vector3.SmoothDamp(cameraTransform.position, targetPosition, ref velocity, smoothTime);
            cameraTransform.LookAt(position);
            if (Vector3.Distance(cameraTransform.position, targetPosition) < 0.01f)
            {
                StartOrbiting();
            }
        }

        public void StartOrbiting()
        {
            menuController.ShowOpenButton();
            GetStartingOrbitAngles();
            SetState(new OrbitingCameraState(this));
        }
        
        public void ContinueOrbiting()
        {
            GetStartingOrbitAngles();
            SetState(new OrbitingCameraState(this));
        }

        public void FocusOnWheels()
        {
            SetState(new FocusingOnWheelsCameraState(this));
        }
        
        public void FocusOnSpoilers()
        {
            SetState(new FocusingOnSpoilersCameraState(this));
        }

        float ClampEulerAngle(float angle, float min, float max)
        {
            if (angle < -360)
                angle += 360;
            if (angle > 360)
                angle -= 360;
            return Mathf.Clamp(angle, min, max);
        }


    }
}