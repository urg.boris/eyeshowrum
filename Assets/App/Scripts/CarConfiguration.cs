﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace EyeShowRum.Scripts
{
    [CreateAssetMenu(fileName = "carConfiguration", menuName = "carConfiguration", order = 0)]
    public class CarConfiguration : ScriptableObject
    {
        [field: SerializeField] public int ColorIndex { get; private set; }
        [field: SerializeField] public int GlossinessIndex { get; private set; }
        [field: SerializeField] public int WheelsIndex { get; private set; }
        [field: SerializeField] public int TuningPart1Index { get; private set; }

        [SerializeField] private CarOptions carOptions;

        public void SetInit()
        {
            ColorIndex = 0;
            GlossinessIndex = 0;
            WheelsIndex = 0;
            TuningPart1Index = 0;
        }

        public void SetColorIndex(int index)
        {
            if (index < 0 || index >= carOptions.colors.Length)
            {
                Debug.LogError("color index=" + index + " cant fit in carOptions.colors ");
                return;
            }

            ColorIndex = index;
        }

        public void SetGlossinessIndex(int index)
        {
            if (index < 0 || index >= carOptions.glosinesses.Length)
            {
                Debug.LogError("Glossiness index=" + index + " cant fit in carOptions.glosinesses ");
                return;
            }

            GlossinessIndex = index;
        }


        public void SetWheelIndex(int index)
        {
            if (index < 0 || index >= carOptions.wheels.Length)
            {
                Debug.LogError("wheel index=" + index + " cant fit in carOptions.wheels ");
                return;
            }

            WheelsIndex = index;
        }

        public void SetTuningPart1Index(int index)
        {
            if (index < 0 || index >= carOptions.tuningPart1Options.Length)
            {
                Debug.LogError("tuningpart1 index=" + index + " cant fit in carOptions.tuningPart1Options ");
                return;
            }

            TuningPart1Index = index;
        }

        public float CountPrice()
        {
            float sum =
                carOptions.colors[ColorIndex].price * carOptions.glosinesses[GlossinessIndex].priceCoef +
                carOptions.wheels[WheelsIndex].price +
                carOptions.tuningPart1Options[TuningPart1Index].price;
            return sum;
        }
    }
}