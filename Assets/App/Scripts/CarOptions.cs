﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace EyeShowRum.Scripts
{
    
    [Serializable]
    public class CarWheelsOptions
    {
        public string name;
        public float price;
    }

    [Serializable]
    public class CarColorOptions
    {
        public string name;
        public Color color;
        public float price;
    }
    
    [Serializable]
    public class TuningPart1Options
    {
        public string name;
        public float price;
    }
    [Serializable]
    public class CarGlosiness
    {
        public string name;
        public float metallicCoef;
        public float priceCoef;
    }
    
    [CreateAssetMenu(fileName = "carOptions", menuName = "carOptions", order = 0)]
    public class CarOptions : ScriptableObject
    {
        public CarColorOptions[] colors;
        public CarGlosiness[] glosinesses;
        public CarWheelsOptions[] wheels;
        public TuningPart1Options[] tuningPart1Options;
    }


}