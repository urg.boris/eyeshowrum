﻿using UnityEngine;
using UnityEngine.UI;

namespace EyeShowRum.Scripts
{
    public class ColorChoiceHandle : MonoBehaviour
    {
        public Image image;

        public void SetColor(Color color)
        {
            image.color = color;
        }
    }
}