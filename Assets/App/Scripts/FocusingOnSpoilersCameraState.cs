﻿


using UnityEngine;

namespace EyeShowRum.Scripts
{
    public class FocusingOnSpoilersCameraState: ACameraState
    {
        private Vector3 velocity = Vector3.zero;
        private Quaternion finalCameraRot;
        public FocusingOnSpoilersCameraState(CameraController cameraController) : base(cameraController)
        {
            finalCameraRot = Quaternion.Euler((cameraController.xAngleLimits[0]+cameraController.xAngleLimits[1])/2, 0, 0);
        }

        public override void Update()
        {
            var position = cameraController.orbitingCenter.position;
            var targetPosition = position + finalCameraRot * new Vector3(0.0f, 0.0f, -cameraController.radius);
            
            cameraController.cameraTransform.position = Vector3.SmoothDamp(cameraController.cameraTransform.position, targetPosition, ref velocity, cameraController.smoothTime);
            cameraController.cameraTransform.LookAt(position);
            if (Vector3.Distance(cameraController.cameraTransform.position, targetPosition) < 0.01f)
            {
                cameraController.ContinueOrbiting();
            }
        }
    }
}