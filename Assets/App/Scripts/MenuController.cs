﻿using System;
using System.Collections.Generic;
using DanielLochner.Assets.SimpleScrollSnap;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace EyeShowRum.Scripts
{
    public class MenuController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] private AppController appController;
        [SerializeField] private SimpleScrollSnap simpleScrollSnap;
        [SerializeField] private GameObject colorPrefab;
        [SerializeField] private Dropdown glossinessDropdown;
        [SerializeField] private Dropdown wheelsDropdown;
        [SerializeField] private Dropdown tuning1Dropdown;
        [SerializeField] private GameObject menuPanel;
        [SerializeField] private Button openButton;

        private bool mouseOver = false;

        public Text price;


        public void Start()
        {
            menuPanel.SetActive(false);
            openButton.gameObject.SetActive(false);
        }

        public void ShowOpenButton()
        {
            openButton.gameObject.SetActive(true);
        }

        public void OpenMenu()
        {
            menuPanel.SetActive(true);
            openButton.gameObject.SetActive(false);
        }

        public void UpdatePrice(float priceSum)
        {
            price.text = priceSum.ToString();
        }


        public void SetOptions(CarOptions carOptions)
        {
            SetColorOptions(carOptions.colors);
            SetGlossinessOptions(carOptions.glosinesses);
            SetWheelOptions(carOptions.wheels);
            SetTuningPart1Options(carOptions.tuningPart1Options);
        }

        void SetColorOptions(CarColorOptions[] colors)
        {
            foreach (var oneCarColorChoice in colors)
            {
                var newColor = simpleScrollSnap.AddToBack(colorPrefab);
                ColorChoiceHandle colorChoiceHandle = newColor.GetComponent<ColorChoiceHandle>();
                colorChoiceHandle.SetColor(oneCarColorChoice.color);
            }

            ColorChanged(0, 0);
        }

        void SetGlossinessOptions(CarGlosiness[] options)
        {
            glossinessDropdown.ClearOptions();
            List<string> dropDownOption = new List<string>();
            foreach (var option in options)
            {
                dropDownOption.Add(option.name);
            }

            glossinessDropdown.AddOptions(dropDownOption);
        }

        void SetWheelOptions(CarWheelsOptions[] options)
        {
            wheelsDropdown.ClearOptions();
            List<string> dropDownOption = new List<string>();
            foreach (var option in options)
            {
                dropDownOption.Add(option.name);
            }

            wheelsDropdown.AddOptions(dropDownOption);
        }

        void SetTuningPart1Options(TuningPart1Options[] options)
        {
            tuning1Dropdown.ClearOptions();
            List<string> dropDownOption = new List<string>();
            foreach (var option in options)
            {
                dropDownOption.Add(option.name);
            }

            tuning1Dropdown.AddOptions(dropDownOption);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            mouseOver = true;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            mouseOver = false;
        }

        public void ColorChanged(int colorIndex, int i2)
        {
            appController.ChangeCarMaterialColor(colorIndex);
        }

        public bool IsMouseOver()
        {
            return mouseOver;
        }
    }
}