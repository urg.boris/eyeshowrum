﻿using UnityEngine;

namespace EyeShowRum.Scripts
{
    public class OrbitingCameraState: ACameraState
    {
        public OrbitingCameraState(CameraController cameraController) : base(cameraController)
        {
            
        }

        public override void Start()
        {

        }

        public override void Update()
        {
            cameraController.OrbitingUpdate();
        }
        
    }
}