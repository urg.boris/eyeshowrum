﻿using System;
using UnityEngine;

namespace EyeShowRum.Scripts
{
    public class PartSwitcher : MonoBehaviour
    {
        [SerializeField] private  GameObject[] parts;
        private int currentPartIndex = 0;
        private void Start()
        {
            currentPartIndex = 0;
            ShowCurrentPart();
        }

        public bool IsValid(int optionsForPartLength)
        {
            if (optionsForPartLength != parts.Length)
            {
                Debug.LogError(name+" doest reflect parts from config: "+parts.Length+" != "+optionsForPartLength);
                return false;
            }

            return true;
        }

        public void ChangePart(int index)
        {
            currentPartIndex = index;
            ShowCurrentPart();
        }

        void ShowCurrentPart()
        {
            for (int i = 0; i < parts.Length; i++)
            {
                parts[i].SetActive(i==currentPartIndex);
            }
        }
    }
}