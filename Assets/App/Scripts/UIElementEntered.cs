﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace EyeShowRum.Scripts
{
    public class UIElementEntered : MonoBehaviour, IPointerEnterHandler,
            IPointerExitHandler
    {
        public UnityEvent Callback;
        public void OnPointerEnter(PointerEventData eventData)
        {
            Callback?.Invoke();
        }
        
        public void OnPointerExit(PointerEventData eventData)
        {
            
        }
    }
}