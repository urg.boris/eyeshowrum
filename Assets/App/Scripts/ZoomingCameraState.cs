﻿
namespace EyeShowRum.Scripts
{
    public class ZoomingCameraState: ACameraState
    {
        public ZoomingCameraState(CameraController cameraController) : base(cameraController)
        {
            
        }

        public override void Update()
        {
            cameraController.ZoomingUpdate();
        }
    }
}